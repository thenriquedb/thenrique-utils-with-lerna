# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.5.1](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.5.0...v1.5.1) (2021-09-30)


### Bug Fixes

* new package name ([cff0b0e](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/cff0b0eddc958b74a0637987669adb8b97d5c003))





# [1.5.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.4.0...v1.5.0) (2021-09-30)


### Features

* newFeature3 ([84fa2b3](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/84fa2b3d111af7b6719db60fdb2435ca6538d2d9))





# [1.4.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.3.1...v1.4.0) (2021-09-30)


### Features

* soco ([e64fed8](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/e64fed864d629df2dcd8b0aa1143947be1a79957))





## [1.3.1](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.3.0...v1.3.1) (2021-09-30)


### Bug Fixes

* asjkhakjsahujks.asa ([fcd6db8](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/fcd6db844ef7a4862b3f22312d9b1a46b8aba144))
* desgra;ca ([5621fe5](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/5621fe51437859a6a6438433e730b3802d93180c))
* ok ([b37fe65](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/b37fe65c6f270816ea4a60e51eab5b2f5a5139db))


### Features

* 1212 ([297a1a0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/297a1a06311fae535c8a0388a10a62302f1bc849))
* another ([433c1a8](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/433c1a889bac39f3b101150a887fa9f50eb19cd9))
* desgra;ca ([5d59c47](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/5d59c47cb0f34738cc88077455c83dce07fd36fb))
* meu deus ([c7b7a1e](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/c7b7a1ef03b994e2ef2aa6f60c19a64dfa1d7ed2))





# [1.3.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.2.2...v1.3.0) (2021-09-29)


### Features

* socodad ([1798be7](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/1798be7aec74ce6bcf069fc9d783b4c30bd83d86))





## [1.2.2](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.2.1...v1.2.2) (2021-09-29)


### Bug Fixes

* sdsd ([040ea2b](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/040ea2b70ff01a72b0087f010d8759448a5816c9))





## [1.2.1](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.2.0...v1.2.1) (2021-09-29)


### Bug Fixes

* adadasas ([c6bd833](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/c6bd833b5fbbd35138149b63a21e5904e9b81eb5))


### Features

* aaaaaaaaaaa ([8db2f17](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/8db2f17efadefa58936b44a470771a0e2df51134))





# [1.2.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.1.0...v1.2.0) (2021-09-29)


### Features

* new feature 2 ([68fcbe3](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/68fcbe3683712bed06c713b335e4bc80b793d1a1))
* socorro ([b39c04e](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/b39c04e31e29fbd2956e6f4e594caa3acd7816e3))





# [1.1.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.0.2...v1.1.0) (2021-09-29)


### Features

* configure ci to install lerna as global package ([8c647c4](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/8c647c43cc07007273462219132dfd8989339419))
* new feature 01 ([a2c7095](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/a2c7095819d1e355544680f1c3a22a554be28cea))





## [1.0.2](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.0.1...v1.0.2) (2021-09-29)


### Bug Fixes

* cli config ([0cabe64](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/0cabe641d90fe54c5678863de88e45ff522b6fc3))
* erro on release script ([1fda4e3](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/1fda4e35f48946a220e51c95f52aa71f7f02ec39))
* gitlab-ci ([1f7de7c](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/1f7de7c7872f09178aef0f52e031b6ba505b280c))
* update .gitlab-ci.yml file ([c480329](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/c48032925eac035939630567d197602c438486ee))





## 1.0.1 (2021-09-29)


### Bug Fixes

* lerna config ([14c26bf](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/14c26bf69a47208da2e273817c75463d045b2407))


### Features

* init ([19a0676](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/19a067669559bb38b8dd70a1b48b058328b912c2))
