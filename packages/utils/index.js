function welcome(name) {
  return `Welcome ${name}!`;
}

function getDate() {
  return new Date().getDate().toString();
}

function newFeature1() {
  console.log("New feature 01");
}

function newFeature2() {
  console.log("New feature 2");
}
function newFeature3() {
  console.log("New feature 3");
}

module.exports = { welcome, getDate, newFeature1, newFeature2, newFeature3 };
