# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.5.1](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.5.0...v1.5.1) (2021-09-30)


### Bug Fixes

* new package name ([cff0b0e](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/cff0b0eddc958b74a0637987669adb8b97d5c003))





# [1.5.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.4.0...v1.5.0) (2021-09-30)


### Features

* newFeature3 ([84fa2b3](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/84fa2b3d111af7b6719db60fdb2435ca6538d2d9))





# [1.4.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.3.1...v1.4.0) (2021-09-30)


### Features

* soco ([e64fed8](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/e64fed864d629df2dcd8b0aa1143947be1a79957))





## [1.3.1](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.3.0...v1.3.1) (2021-09-30)


### Bug Fixes

* ok ([b37fe65](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/b37fe65c6f270816ea4a60e51eab5b2f5a5139db))





# [1.3.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.2.2...v1.3.0) (2021-09-29)


### Features

* socodad ([1798be7](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/1798be7aec74ce6bcf069fc9d783b4c30bd83d86))





## [1.2.2](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.2.1...v1.2.2) (2021-09-29)

**Note:** Version bump only for package @thenriquedb/utils-v2





## [1.2.1](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.2.0...v1.2.1) (2021-09-29)


### Bug Fixes

* adadasas ([c6bd833](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/c6bd833b5fbbd35138149b63a21e5904e9b81eb5))





# [1.2.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.1.0...v1.2.0) (2021-09-29)


### Features

* new feature 2 ([68fcbe3](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/68fcbe3683712bed06c713b335e4bc80b793d1a1))





# [1.1.0](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.0.2...v1.1.0) (2021-09-29)


### Features

* new feature 01 ([a2c7095](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/a2c7095819d1e355544680f1c3a22a554be28cea))





## [1.0.2](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/compare/v1.0.1...v1.0.2) (2021-09-29)


### Bug Fixes

* cli config ([0cabe64](https://gitlab.com/thenriquedb/thenrique-utils-with-lerna/commit/0cabe641d90fe54c5678863de88e45ff522b6fc3))





## 1.0.1 (2021-09-29)

**Note:** Version bump only for package @thenriquedb/utils
